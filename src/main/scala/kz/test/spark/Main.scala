package kz.test.spark

import org.apache.spark.sql.SparkSession

object Main {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Spark test").getOrCreate()
    import spark.implicits._
    val df = spark.read.textFile("README.md")
    val words = df.flatMap(_.split(" "))
    val result = words.groupByKey(identity).count.toDF("word", "qty").sort($"qty".desc)
    result.repartition(1).write.csv("test/df")
    spark.stop()
  }

}
